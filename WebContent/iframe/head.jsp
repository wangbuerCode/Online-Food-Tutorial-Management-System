<%@ page language="java" import="java.util.*"  contentType="text/html;charset=gb2312" %> 
<jsp:useBean id="cb" scope="page" class="com.bean.ComBean"/> 
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/"; 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="<%=basePath %>images/style.css" rel="stylesheet" type="text/css">  
<script type="text/javascript" src="<%=basePath %>images/scroll_jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>images/jquery.js"></script>
<script type="text/javascript" src="<%=basePath %>images/fun.js"></script>
<script type="text/javascript" src="<%=basePath %>images/imgSlide.js"></script>
<script type="text/javascript" src="<%=basePath %>images/kxbdMarquee.js"></script>
<script type="text/javascript" src="<%=basePath %>images/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<%=basePath %>images/ad.js"></script>
<script type="text/javascript">
	$(function(){
		/*$('#honorlist').kxbdMarquee({direction:'left'});*/
		$('#ztcenlist').kxbdMarquee({direction:'left'}); 
	});
</script>
<style>
#ztcenlist {width: 728px;height: 246px;float: right;position: relative;overflow: hidden;}
#ztcenlist li {width: 256px;height: 246px;float: left;padding: 4px 8px 0 8px;display: inline;}
#ztcenlist li p {width: 256px;overflow: hidden;}
#ztcenlist li p img {width: 256px;height: 212px;}
#ztcenlist li h3 {
	width: 256px;
	height: 20px;
	line-height: 20px;
	font-weight: normal;
	font-size: 12px;
	text-align: center;
}
.case-cen {
	width: 728px;
	/*height: 146px;*/
	float: left;
	position: relative;
	overflow: hidden;
	background: #FFF;
}
</style>

<script type="text/javascript" charset="UTF-8" src="<%=basePath %>images/bsl.js"></script>
<script type="text/javascript" charset="UTF-8" src="<%=basePath %>images/main_icon_invite_mess_api.js"></script>
<link rel="stylesheet" type="text/css" href="<%=basePath %>images/main.css">
<link rel="stylesheet" type="text/css" href="<%=basePath %>images/fix.css">
<link type="text/css" rel="stylesheet" href="<%=basePath %>images/m-webim-lite.css">
<script type="text/javascript" src="<%=basePath %>images/m-webim-lite.js" charset="utf-8"></script>
<link rel="stylesheet" href="<%=basePath %>images/float.css" type="text/css" charset="utf-8">
</head>
<body style="">
<div class="layout">
<div class="header">
  <div class="logo"><br><p style="font-family:Microsoft YaHei;font-weight:bold;font-size:300%;color:#197ac1">在线学美食网站</p></div>
  <div class="sitetool"> </div>
  <div class="clear"></div>
</div>
<div class="navlist">
  <div class="nav">
    <ul>
      <li><h3><a href="<%=basePath%>index.jsp">网站首页</a></h3></li>  
      <li><h3><a href="<%=basePath%>cp.jsp">美食信息</a></h3></li> 
      <li><h3><a href="<%=basePath%>s.jsp">美食搜索</a></h3></li>   
      <li><h3><a href="<%=basePath%>member/index.jsp">用户中心</a></h3></li>  
      <li><h3><a href="<%=basePath%>admin/index.jsp">管理登录</a></h3></li> 
    </ul> 
    <div class="clear"></div>
  </div>
</div>
<%
String message = (String)request.getAttribute("message");
	if(message == null){
		message = "";
	}
	if (!message.trim().equals("")){
		out.println("<script language='javascript'>");
		out.println("alert('"+message+"');");
		out.println("</script>");
	}
	request.removeAttribute("message");
%>